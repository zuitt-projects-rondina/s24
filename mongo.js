// QUERY OPERATORS AND FIELD PROJECTION

/*MINI ACTIVITY*/

db.products.insertMany([
				{
					"name" : "Iphone X",
					"price" :  30000,
					"isActive" : true
				},
				{
					"name" : "Samsung Galaxy S21",
					"price" :  51000,
					"isActive" : true
				},
				{
					"name" : "Razer Blackshark V2X",
					"price" :  2800,
					"isActive" : false
				},
				{
					"name" : "RAKK Gaming Mouse",
					"price" :  1800,
					"isActive" : true
				},
				{
					"name" : "Razer Mechanical Keyboard",
					"price" :  4000,
					"isActive" : true
				}


	])


// QUERY OPERATORS
		// allow for more flexible querying in MongoDB
		// instead of just being able to find or search for documents with exact value
		// we can use define conditions instead of just specific criteria


// $gt, $lt , $gte amd $lte - numbers
		
		// $gt- greater than

		db.products.find({"price" :  {$gt : 3000}})

		// $ gte - greater than or equal to 
		db.products.find({"price" :  {$gte : 30000}})

		// $lt - less than
		db.products.find({"price" :  {$lt : 4000}})

		// $lte - less than or equal to
		db.products.find({"price" :  {$lte : 2800}})


// NOTES : QUERY OPERATORS can be used to expanfd the search criteria for updating and deleting


		db.products.updateMany({"price" : {$gte : 30000}}, {$set : {"isActive" : false}})


db.users.insertMany([
			{
				"firstName": "Mary Jane",
				"lastName": "Watson",
				"email" : "mjtiger@gmail.com",
				"password" : "tigerjackpo15",
				"isAdmin" : false
			},
			{
				"firstName": "Gwen",
				"lastName": "Stacy",
				"email" : "stacyTech@gmail.com",
				"password" : "stcyTech1991",
				"isAdmin" : true
			},
			{
				"firstName": "Peter",
				"lastName": "Parker",
				"email" : "peterWebDev@gmail.com",
				"password" : "webDeveloperPeter",
				"isAdmin" : true
			},
			{
				"firstName": "Jonah",
				"lastName": "Jameson",
				"email" : "jjjameson@gmail.com",
				"password" : "spideyIsamenace",
				"isAdmin" : false
			},
			{
				"firstName": "Otto",
				"lastName": "Octavius",
				"email" : "ottoOctopi@gmail.com",
				"password" : "docOck15",
				"isAdmin" : true 
			}


	])

// $regex - this query operator will allows us to find documents in which it will match the characters/ pattern of the character we are looking for



	db.users.find({"firstName" : {$regex : 'O'}})
	db.users.find({"firstName" : {$regex : 'W'}})

// $options - for case sensitivity
	
	db.users.find({"lastName" : {$regex : 'o', $options : '$i'}})


// FIND DOCUMENTS THAT MATCHES SPECIFIC WORD
	db.users.find({"email" : {$regex : 'web', $options : '$i'}})
	db.products.find({"name" : {$regex : 'phone', $options : '$i'}})
/*MINI ACTIVITY*/


	db.products.find({"name" : {$regex : 'razer', $options : '$i'}})

	db.products.find({"name" : {$regex : 'rakk', $options : '$i'}})


// $or and $and

	//$or operator - allow us to have a logical operation wherein wecan look or find for a local document which can satisfy at least one of the condition

	db.products.find({ 

		$or : [
				{"name" : {$regex : 'x', $options : '$i'}},
				{"price" : {$lte :10000}}
		]

	})

	db.products.find({
			$or : [
					{"name" : {$regex : 'x', $options : '$i'}},
					{"price" : 30000}

			]


	})

	db.users.find({
			$or : [
					{"firstName" : {$regex : 'a', $options: '$i'}},
					{ "isAdmin" : true}
			]

	})

	/*MINI ACTIVITY*/

	db.users.find({
			$or : [
					{"lastName" : {$regex : 'w', $options: '$i'}},
					{ "isAdmin" : false}
			]

	})


// $and operator
	// allow us to have a logical operator where we can look for document which can satisfy both conditions


	db.products.find({
		$and : [
			{"name": {$regex : 'razer', $options : '$i'}},
			{"price" : {$gte : 3000}}

		]
	})

	db.users.find({
			$and : [
					{"lastName" : {$regex : 'w', $options: '$i'}},
					{ "isAdmin" : false}
			]

	})

	/*MINI ACTIVITY*/
	db.users.find({
			$and : [
					{"lastName" : {$regex : 'a', $options: '$i'}},
					{"firstName" : {$regex : 'e', $options: '$i'}}
					
			]

	})


// Field Projection
	// allow us to hide / show properties/ field of the returned document after a query

	// 0 means hide and 1 means show
	// db.collection.find ({query}, {"field" : 0/1})

	db.users.find({}, {"_id": 0, "password" : 0})

	// find() can have two arguments,
			// db.collection.find ({query}, {projection})


	db.users.find({"isAdmin" : false}, {"_id" : 0, "email" : 1})

	db.products.find({"price" : {$gte :10000}}, 
					{"_id" : 0, "name" : 1, "price" : 1})
